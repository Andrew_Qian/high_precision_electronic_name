
#include "main.h"

//当前读取的重量
float now_weight = 0;
//触摸屏输入的单价
int price = 0;
//计算金额
float money = 0;
//累加金额的时候，数组存放之前的金额
float add_money = 0;
//格式化缓冲区
char string_buf[50];


//判断低功耗需要的变量
float low_power_weight_1 = 0;
float low_power_weight_2 = 0;
unsigned char low_power_num = 0;


int main(void)
{
	USART_1_Init(9600);   //串口发送
	
	USART_RX_DMA_Config();	//配置串口接收端DMA
	
	Beep_Init();			//蜂鸣器作为报警提示
	
	Key_Init();				//按键唤醒停机模式
		
	CS1237_Init();			//CS1237初始化
	
	Get_Weight_Coe();		//获取称重系数和上电时称得的重量，后面都减去这个值
	
	HMI_Send_Start();		//先发送一次，确保和触摸屏的正常通信
	
	//先给显示屏一个值，别人人家等急了，以为东西坏的
	sprintf(string_buf,"t0.txt=\"0\"");
	HMI_Send_String(string_buf);
	Delay_ms(20);
	sprintf(string_buf,"t2.txt=\"0\"");
	HMI_Send_String(string_buf);

	while(1)
	{
		//读取实时重量并刷新
		now_weight = Get_Weight();
		
		//检测超出量程报警
		if(now_weight >= 490)
			Beep_Warning(3,100);
		sprintf(string_buf,"t4.txt=\"%.1f\"",now_weight);
		HMI_Send_String(string_buf);
		
		//计算价格并刷新
		//累加上次的价格，没有按下累加按键的时候，前几次的价格都为0
		money = now_weight * price + add_money;		
		sprintf(string_buf,"t2.txt=\"%.1f\"",money);
		HMI_Send_String(string_buf);
		
		//因为9600的波特率，比较低，这里延时等待数据发送完毕，不然后面一直进入串口中断影响后面的发送和接收。
		Delay_ms(10);
		
		//查看DMA缓冲区有无数据
		if(USART_DMA_Receive() == 1)
			HMI_Receive();
		
		//下面是关于进入低功耗的判断
		// 仿真发现在没有收到触摸屏的按下时，循环一次的时间大致为4s，这样省去了一个定时器，避免了中断
		low_power_weight_1 = now_weight;
		if((low_power_weight_1 < 1) && ((low_power_weight_1-low_power_weight_2 < 1) || (low_power_weight_2-low_power_weight_1 < 1)))
		{
			low_power_num++;
		}
		else
		{
			low_power_num = 0;
			low_power_weight_2 = low_power_weight_1;
		}
		
		//重量低于1g并且在40秒内没有变化，即开始进入低功耗
		if(low_power_num >= 80)
		{
			//计数清零，准备下一次的计数
			low_power_num = 0;
			//蜂鸣器首先响用来提示
			Beep_Warning(3,300);		
			//进入低功耗
			Low_Power_Mode();
		}

	}	
}

