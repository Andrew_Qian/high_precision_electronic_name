
#include "main.h"

extern int price ;
extern float money ;
extern float add_money ;
extern unsigned char DMA_Receive_LEN ;


/*
** 确保串口HMI正常通信
*/
void HMI_Send_Start(void)
{
	Delay_ms(30);
	HMI_Send_String("'\0'");
	Delay_ms(30);
}


/*---------------------------------
** 名称：HMI_Send_String
** 功能：中断方式发送字符串给串口屏，包括结束字符
** 入口：需要发送的数据的指针
** 出口：无
** 说明：先把数据拷贝到缓冲区，然后开启串口中断，主程序可以继续执行
----------------------------------*/
void HMI_Send_String(char *buf1)
{
	u8 i = 0;
	
	while(buf1[i] != '\0')
	{
		Tx_Buffer[Tx_Counter++] = *(buf1+i);
		i++;
	}
	
	Tx_Buffer[Tx_Counter++] = 0XFF;
	Tx_Buffer[Tx_Counter++] = 0XFF;
	Tx_Buffer[Tx_Counter++] = 0XFF;
	
	if(!(USART1->CR1 & USART_CR1_TXEIE))					//中断方式发送
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE); 	//只要发送寄存器空，就会一直有中断
}

/*---------------------------------
** 名称：HMI_Receive
** 功能：根据返回内容，分情况处理
** 入口：无
** 出口：无
** 说明：这里需要和串口屏的配置相互协调，规定好发送的字符和格式
----------------------------------*/
void HMI_Receive(void)
{
	
	//前两个数据是0x65和页面ID，已经在中断接受中处理
	switch(CmdRx_Buffer[2])
	{
		//去皮
		case 0x72:
			remove_empty_weight();
			break;
			
		//累加金额
		case 0x61:
			add_money = money;
			price = 0;
			break;
		
		//确认单价
		case 0x64:
			if(DMA_Receive_LEN == 5)
				price = CmdRx_Buffer[DMA_Receive_LEN-2] - 48;
			else if(DMA_Receive_LEN == 6)
				price = (CmdRx_Buffer[DMA_Receive_LEN-3]-48)*10 + (CmdRx_Buffer[DMA_Receive_LEN-2]-48);
			else if(DMA_Receive_LEN == 7)
				price = (CmdRx_Buffer[DMA_Receive_LEN-4]-48)*100 + (CmdRx_Buffer[DMA_Receive_LEN-3]-48)*10 + (CmdRx_Buffer[DMA_Receive_LEN-2]-48);	
			break;
		
		//清除单价和金额
		case 0x63:
			price = 0;
			add_money = 0;
			break;

		default :break;		
	}
	//清空缓冲寄存器
	memset(CmdRx_Buffer,0,sizeof(CmdRx_Buffer));

}

//HMI息屏
void HMI_Sleep_Mode(void)
{
	char buf[10] = {0};
	
	sprintf(buf,"sleep=1");
	HMI_Send_String(buf);
	Delay_ms(20);
}

//HMI退出息屏
void HMI_Unsleep_Mode(void)
{
	char buf[10] = {0};
	
	sprintf(buf,"sleep=0");
	HMI_Send_String(buf);
	Delay_ms(20);
}

