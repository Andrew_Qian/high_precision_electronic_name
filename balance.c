/*
** 此文件包含称重需要的函数
** 首先保存净重和500g的重量，计算出称重系数
** 这个里面实现的去皮重，其实就是先把比如说托盘放在称上面，称出托盘的净重，
	把这个净重系数更新到 empty_cow 变量中，然后后面计算的时候，减去的皮重就是
	这个托盘的重量。
** 在没有开启去皮重之前，其实程序中还是减去了一个皮重的，只不过这个皮重是校准量，
	使称在开机的时候，显示 0g。
	
** 称重的时候注意关中断
*/

#include "main.h"


//在确定程序之前，检测这两个ADC
//有的程序是在程序开始之前，读取两次，然后把他们存放在eeprom里面
const long int weight_empty = 0;
const long int weight_500g = 0;

//称重系数，即直线的斜率
const float weight_coe = 5.22;
//皮重，在按下去皮按键的时候，数值会被修改
float empty_coe = 0;


/*---------------------------------
** 名称：Get_Weight_Coe
** 功能：开机时读取当前重量，作为后面重量的基准点
** 入口：无
** 出口：无
** 说明：在主函数中作为称重系数的初始化
----------------------------------*/
void Get_Weight_Coe(void)
{
	unsigned char filter_num = 10;
	unsigned long int temp = 0;
	
	while(filter_num--)
	{
		temp += CS1237_Read_18bit_ADC();
	}	
	temp = temp / 10;
	empty_coe = weight_coe * temp;
}

/*---------------------------------
** 名称：Get_Weight
** 功能：根据读取的CS1237 ADC值计算实际重量
** 入口：无
** 出口：重量，单位：g
** 说明：主函数中实时计算重量
----------------------------------*/
float Get_Weight(void)
{
	float dat = 0;
	unsigned long int dat_2 = 0;
	float dat_3 = 0;
	
	//读取ADC，并转化为单位：克
	dat = filter_eliminate_dithering(CS1237_Read_18bit_ADC());
	dat *= weight_coe;
	dat -= empty_coe;
	
	if(dat < 0)
		dat = 0;
	
	//前面K放大了1000倍，这里先除10，为了小数点后面的四舍五入
	dat_2 = dat / 10;
	//dat_2 /= 100;
	if(dat_2 % 100 > 5)
	{
		dat_2 /= 10;
		dat_2 += 1;
	}
	else
		dat_2 /= 10;
	//转换为小数输出
	dat_3 = (float)dat_2 / 10.00f;
	return dat_3;
	
}

/*---------------------------------
** 名称：remove_empty_weight
** 功能：去皮重功能，其实就是重新更新一下 empty_coe 参数
** 入口：无
** 出口：无
** 说明：无
----------------------------------*/
void remove_empty_weight(void)
{
	unsigned char remove_filter_num =  2;
	unsigned long int temp = 0;
	
	while(remove_filter_num--)
	{
		temp += CS1237_Read_18bit_ADC();
	}	
	temp = temp / 2;
	empty_coe = weight_coe * temp;
}


