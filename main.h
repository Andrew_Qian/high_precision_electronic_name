
#ifndef __MAIN_H
#define __MAIN_H

#include <stm32f10x.h>
#include <stdio.h>
#include <string.h>

#include "stm32f10x_it.h"
#include "hardware.h"
#include "systick.h"
#include "delay.h"
#include "filter.h"

#include "cs1237.h"
#include "balance.h"
#include "usart_1.h"
#include "key.h"
//#include "TIME2.h"
#include "beep.h"
#include "HMI.h"
#include "power.h"


#endif

